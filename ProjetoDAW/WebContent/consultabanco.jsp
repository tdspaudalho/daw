<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<!-- //@page import="java.sql.Connection"% //@page import="java.sql.DriverManager"% //@page import="java.sql.SQLException"% --> 
<%@page import="java.sql.*"%> 
<%@page import="java.io.*"%> 
<%@page import="javax.servlet.*"%>
<%@page import="persistence.*"%>
 
<%
	Conexao conn = new Conexao();
	//String x = conn.getConexao();
	// Passo 1. Carregar o Driver JDBC no Eclipse
	//Class.forName("org.gjt.mm.mysql.Driver");
	Class.forName("com.mysql.jdbc.Driver");
	
	// Passo 2. Criar um objeto Connection 
	Connection con = DriverManager.getConnection("jdbc:mysql://localhost/teste?user=root&password=Kuruminbrasil1");
	out.println("Conexao iniciada...<br />");
	
	// Passo 3. Criar um objeto Statement 
	Statement s = con.createStatement();
	//Statement ss = con.createStatement();
	//Conexao conex = conn.getConexao();
	
	// Passo 4. Usar o objeto Statement para obter um objeto ResultSet
	String inserir = "INSERT INTO projeto (nome) VALUES ('Beta')";
	s.executeUpdate(inserir);
	
	String sql = "SELECT * FROM projeto";
	ResultSet rs = s.executeQuery(sql);
	
	//Passo 5. Imprimindo o resultado
	while (rs.next()) { out.println(rs.getString(1) + " " + rs.getString(2) + "<br />"); }
	
	//Passo 6. Fechando conexoes
	rs.close(); s.close(); con.close();
%>
</body>
</html>