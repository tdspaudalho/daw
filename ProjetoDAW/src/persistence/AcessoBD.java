package persistence;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AcessoBD
 */
@WebServlet("/acessobd")
public class AcessoBD extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AcessoBD() {
        super();
        
	
		
		
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		Conexao conn = new Conexao();
        conn.getConexao();
		String status = conn.statusConexao();
		System.out.println(status);
		
		PrintWriter imprimir = response.getWriter();
        imprimir.println(status);
		
	}

}
